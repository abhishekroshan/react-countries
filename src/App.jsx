import { useEffect, useState, useContext } from "react";
import "./App.css";
import Card from "./components/card/Card";
import Loading from "./components/loading/Loading";
import NetworkError from "./components/networkError/NetworkError";
import SearchBox from "./components/searchBox/SearchBox";
import SearchDropdown from "./components/searchDropdown/SearchDropdown";
import SubRegionDropdown from "./components/subRegionDropdown/SubRegionDropdown";
import Error from "./components/error/Error";
import PopulationSort from "./components/populationSort/PopulationSort";
import AreaSort from "./components/areaSort/AreaSort";
import Currency from "./components/currencyBox/Currency";

import { ThemeContext } from "./Theme";

function App() {
  const { theme } = useContext(ThemeContext);

  const [allCountries, setAllCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [networkFault, setNetworkFault] = useState(false);
  const [inputvalue, setInputvalue] = useState("");
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [regionAndSubRegionData, setRegionAndSubRegionData] = useState({});
  const [populationSort, setPopulationSort] = useState("");
  const [areaSort, setAreaSort] = useState("");
  const [allCurr, setAllCurr] = useState([]);
  const [curren, setCurren] = useState("");

  //===============================================================

  const API_URL = "https://restcountries.com/v3.1/all";

  const fetchCountriesData = async () => {
    try {
      const res = await fetch(API_URL);
      if (!res.ok) {
        throw new Error("Error occurred");
      }
      const data = await res.json();
      return data;
    } catch (error) {
      throw new Error("Error Occurred");
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const countriesData = await fetchCountriesData();
        setAllCountries(countriesData);

        const result = countriesData.reduce((accumulator, currentValue) => {
          const region = currentValue.region;
          const subRegion = currentValue.subregion;

          if (!accumulator[region]) {
            accumulator[region] = [];
          }

          if (!accumulator[region].includes(subRegion)) {
            accumulator[region].push(subRegion);
          }

          return accumulator;
        }, {});

        const allCurrencies = countriesData.reduce(
          (accumulator, currentValue) => {
            const currency = currentValue.currencies;

            for (let value in currency) {
              if (!accumulator.includes(currency[value].name)) {
                accumulator.push(currency[value].name);
              }
            }

            return accumulator;
          },
          []
        );

        console.log(`curr`, allCurrencies);
        setAllCurr(allCurrencies);

        console.log(result);
        setRegionAndSubRegionData(result);

        setLoading(false);
      } catch (error) {
        console.log(error);
        setNetworkFault(true);
      }
    };

    fetchData();
  }, []);

  //================================================================

  const valueChange = (value) => {
    setInputvalue(value);
  };

  const regionChange = (value) => {
    setRegion(value);
    setSubRegion("");
  };

  const subRegionChange = (value) => {
    setSubRegion(value);
  };

  //=================================================================

  const handlePopulationSort = (order) => {
    setPopulationSort(order);
    setAreaSort("");
  };

  const handleAreaSort = (order) => {
    setAreaSort(order);
    setPopulationSort("");
  };

  //====================================================================

  const handleCurrency = (value) => {
    setCurren(value);
  };

  //====================================================================

  let content;

  if (loading) {
    if (networkFault) {
      content = <NetworkError />;
    } else {
      content = <Loading />;
    }
  } else if (allCountries.length === 0) {
    content = <Error />;
  } else {
    content = (
      <Card
        countries={allCountries}
        region={region}
        subRegion={subRegion}
        inputvalue={inputvalue}
        populationSort={populationSort}
        areaSort={areaSort}
        currencyy={curren}
      />
    );
  }

  //==================================================================

  return (
    <>
      <div className={`searchSection ${theme === "dark" ? "dark" : ""}`}>
        <SearchBox onSearch={valueChange} />
        <SearchDropdown
          onSearch={regionChange}
          options={Object.keys(regionAndSubRegionData)}
        />
        <SubRegionDropdown
          subregions={regionAndSubRegionData[region]}
          onSearch={subRegionChange}
        />
        <Currency currencyOptions={allCurr} onSearch={handleCurrency} />

        <PopulationSort clicked={handlePopulationSort} />
        <AreaSort clicked={handleAreaSort} />
      </div>
      {content}
    </>
  );
}

export default App;
