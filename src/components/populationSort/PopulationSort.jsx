import React, { useContext } from "react";
import "./populationSort.css";
import { ThemeContext } from "../../Theme";

const PopulationSort = ({ clicked }) => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className={`population-container ${theme === "dark" ? "dark" : ""}`}>
      Population
      <button
        className={`asc ${theme === "dark" ? "dark" : ""}`}
        onClick={() => clicked("asc")}
      >
        Asc
      </button>
      <button
        className={`desc ${theme === "dark" ? "dark" : ""}`}
        onClick={() => clicked("desc")}
      >
        Desc
      </button>
    </div>
  );
};

export default PopulationSort;
