import React, { useContext } from "react";
import "./areaSort.css";
import { ThemeContext } from "../../Theme";

const AreaSort = ({ clicked }) => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className={`area-container ${theme === "dark" ? "dark" : ""}`}>
      Area
      <button
        className={`asc  ${theme === "dark" ? "dark" : ""}`}
        onClick={() => clicked("asc")}
      >
        Asc
      </button>
      <button
        className={`desc  ${theme === "dark" ? "dark" : ""}`}
        onClick={() => clicked("desc")}
      >
        Desc
      </button>
    </div>
  );
};

export default AreaSort;
