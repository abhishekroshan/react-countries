import React from "react";
import { Link } from "react-router-dom";
import "./borderCountries.css";

const BorderCountries = ({ countryCode }) => {
  return (
    <Link to={`/country/${countryCode}`} className="links">
      <div className="borders">{countryCode}</div>
    </Link>
  );
};

export default BorderCountries;
