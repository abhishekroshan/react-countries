import React, { useContext } from "react";
import "./card.css";
import { ThemeContext } from "../../Theme";
import { useNavigate } from "react-router-dom";

const Card = ({
  countries,
  region,
  inputvalue,
  subRegion,
  populationSort,
  areaSort,
  currencyy,
}) => {
  const { theme } = useContext(ThemeContext);
  const navigate = useNavigate();

  let filteredCountries = countries.filter((country) => {
    let currName;

    if (country.currencies) {
      const names = Object.values(country.currencies);
      currName = names[0].name;
    }

    console.log(currName);

    if (
      (!inputvalue ||
        country.name.common
          .toLowerCase()
          .startsWith(inputvalue.toLowerCase())) &&
      (!region || region === country.region) &&
      (!subRegion || subRegion === country.subregion) &&
      (!currencyy || currName === currencyy)
    ) {
      return country;
    }
  });

  if (populationSort === "asc") {
    filteredCountries = [...filteredCountries].sort(
      (a, b) => a.population - b.population
    );
  } else if (populationSort === "desc") {
    filteredCountries = [...filteredCountries].sort(
      (a, b) => b.population - a.population
    );
  } else if (areaSort === "asc") {
    filteredCountries = [...filteredCountries].sort((a, b) => a.area - b.area);
  } else if (areaSort === "desc") {
    filteredCountries = [...filteredCountries].sort((a, b) => b.area - a.area);
  }

  return (
    <div className={`country-component ${theme === "dark" ? "dark" : ""}`}>
      {filteredCountries.map((country) => (
        <div
          className="card-container"
          key={country.name.common}
          onClick={() => navigate(`country/${country.cca3}`)}
        >
          <img src={country.flags.png} alt="flag" className="flag" />
          <div className={`country-details ${theme === "dark" ? "dark" : ""}`}>
            <div className="country-name">{country.name.common}</div>
            <div>
              <div className="country-population">
                <b>Population: </b>
                {country.population}
              </div>
              <div className="country-region">
                <b>Region: </b>
                {country.region}
              </div>
              <div className="country-capital">
                <b>Capital:</b> {country.capital}
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Card;
