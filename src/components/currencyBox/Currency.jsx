import React from "react";
import "./currency.css";

const Currency = ({ onSearch, currencyOptions }) => {
  const currencyChange = (e) => {
    onSearch(e.target.value);
  };

  return (
    <div className="dropdown">
      <select onChange={currencyChange}>
        <option value="">Filter by Currency</option>
        {currencyOptions.map((option, index) => (
          <option key={index} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Currency;
