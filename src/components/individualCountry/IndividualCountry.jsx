import React, { useEffect, useState, useContext } from "react";
import "./individualCountry.css";
import { useNavigate, useParams } from "react-router-dom";
import Border from "../borderCountries/BorderCountries";
import { ThemeContext } from "../../Theme";

const IndividualCountry = () => {
  const { theme } = useContext(ThemeContext);
  const navigate = useNavigate();

  const [searchCountry, setSearchCountry] = useState(null);
  const [borderCountries, setBorderCountries] = useState([]);
  const [nativeName, setNativeName] = useState([]);
  const [currency, setCurrency] = useState([]);
  let arrays;

  const { cca3 } = useParams();

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${cca3}`)
      .then((res) => {
        if (!res.ok) {
          console.log("Error");
        }
        return res.json();
      })
      .then((data) => {
        setSearchCountry(data);

        if (data[0].borders) {
          setBorderCountries(data[0].borders);
        }

        console.log(data[0].name.nativeName);

        arrays = Object.values(data[0].name.nativeName);

        setNativeName(arrays);

        setCurrency(Object.keys(data[0].currencies));
        console.log(`dffdsa`, currency);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [cca3]);
  console.log(`fdsdf`, nativeName);

  return (
    <div>
      {searchCountry && (
        <div className={`individual-country ${theme === "dark" ? "dark" : ""}`}>
          <button
            className={`individual-button ${theme === "dark" ? "dark" : ""}`}
            onClick={() => navigate("/")}
          >
            Go Back
          </button>
          <div className="individual-details">
            <img src={searchCountry[0].flags.png} alt="" />
            <div className="individual-detail">
              <h1 className="name">{searchCountry[0].name.common}</h1>
              <div className="details">
                <div className="details-left">
                  <div>
                    <strong>Native Name: </strong>
                    {nativeName[0].common}
                  </div>
                  <div>
                    <strong>Population: </strong>
                    {searchCountry[0].population}
                  </div>
                  <div>
                    <strong>Region: </strong>
                    {searchCountry[0].region}
                  </div>
                  <div>
                    <strong>SubRegion: </strong>
                    {searchCountry[0].subregion}
                  </div>
                  <div>
                    <strong>Capital: </strong>
                    {searchCountry[0].capital}
                  </div>
                </div>
                <div className="details-right">
                  <div>
                    <strong>Top level Domain: </strong>
                    {searchCountry[0].tld}
                  </div>
                  <div>
                    <strong>Currencies: </strong>
                    {currency[0]}
                  </div>
                </div>
              </div>
              {borderCountries.length !== 0 && (
                <div className="borderCoun">
                  <strong>Border:</strong>
                  {borderCountries.map((countryCode) => (
                    <Border key={countryCode} countryCode={countryCode} />
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default IndividualCountry;
